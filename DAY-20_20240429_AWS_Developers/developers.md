#### Session Video:

```
https://drive.google.com/file/d/1Yr1RcBJ7XTica6z_LVlfW22hNUJ0yMmn/view?usp=sharing
```

#### AWS Developers Tools

```
AWS Services - Developer Tools	
	
    1. AWS Cloud Development Kit (AWS CDK) :
        https://docs.aws.amazon.com/cdk/v2/guide/about_examples.html

	2. AWS CloudShell :
        https://awscli.amazonaws.com/v2/documentation/api/latest/reference/index.html#cli-aws

	3. AWS CodeArtifact:

	4. AWS CodeBuild
	5. AWS CodeCommit
	6. AWS CodeDeploy
	7. AWS CodePipeline

	8. AWS CodeStar
	
    9. AWS Command Line Interface (AWS CLI)
	        https://awscli.amazonaws.com/v2/documentation/api/latest/reference/index.html#cli-aws

    10. AWS SDKs and Tools
            https://docs.aws.amazon.com/cdk/v2/guide/about_examples.html

	11. AWS X-Ray
	
```
