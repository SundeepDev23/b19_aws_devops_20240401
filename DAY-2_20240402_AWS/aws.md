#### Session Video:

```
https://drive.google.com/file/d/1G4ErvUSoVo1qq8qOEAXDLVqjKJQFhXk8/view?usp=sharing
```

#### Getting Started with Basics:

```
Cloud Computing Account Creation:
    1. AWS 

AWS Global Infrastructure:

Global Services vs Region:

Regions : 33 ; AZ's : 105 & CDN/PoPs/EdgeLocations : 600+ & 13 Regional edge caches

Global vs Region Services 

Analytics :
    1. Athena : Amazon Athena : Region Based Service
    2. AWS Clean Rooms

Application Integration
Blockchain
Business Applications
Cloud Financial Management
Compute
Containers
Customer Enablement
Database
Developer Tools
End User Computing
Front-end Web & Mobile
Game Development
Internet of Things
Machine Learning
Management & Governance
Media Services
Migration & Transfer
Networking & Content Delivery
Quantum Technologies
Robotics
Satellite
Security, Identity, & Compliance:
    1. IAM  : Global Service 
Storage

```
#### IAM 
```
IAM :
    - Groups
    - Users
    - Policy 

```

#### cloudbinary-only-iam-policy
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "iam:CreateUser",
                "iam:CreateLoginProfile",
                "iam:AddUserToGroup",
                "iam:AttachUserPolicy",
                "iam:PutUserPolicy",
                "iam:TagUser"
            ],
            "Resource": "*"
        }
    ]
}
```