#### Session Video:
```
https://drive.google.com/file/d/1uUYI8EpPE2tLw0zLDjGv3ZBhFPs2Aa_K/view?usp=sharing
```

AWS Storage Services:
    1. Amazon Elastic Block Store (Amazon EBS)

AWS Compute Services:
    1. Amazon EC2 Auto Scaling
    2. Launch Template
    3. User Data 

AWS Networking Services:
    1. Elastic Load Balancing (ELB)


```

#!/bin/bash

# Setup Hostname
sudo hostnamectl set-hostname "dev.c3ops.io"

# Update the hostname part of Host File
echo "`hostname -I | awk '{ print $1 }'` `hostname`" >> /etc/hosts

# Update Ubuntu Repository
sudo apt-get update

# Download, & Install Utility Softwares
sudo apt-get install git wget unzip curl tree -y

# Download, Install & Configure WebServer on Ubuntu
sudo apt-get update 

sudo apt-get install apache2 -y

# Enable & Start the Apache2 Service
sudo systemctl enable apache2
sudo systemctl restart apache2

# Go to Opt folder
cd /opt/

# Download Source Code from GitHub
sudo git clone https://github.com/kesavkummari/kesavkummari-website-code.git

# Go inside the code and copy the code to DocumentRoot
cp -pvr /opt/kesavkummari-website-code/* /var/www/html/
```