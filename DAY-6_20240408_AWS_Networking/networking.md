#### Session Video:

```
https://drive.google.com/file/d/1NIE8D1j1kx76r2KtqOIDTBgUcIglDy_E/view?usp=sharing
```

#### IAM User - Access Analyser - CloudTrail - Netowrking 

```
https://aws.amazon.com/vpc/
```


```

Cloud Admin/Engineer:

    Console sign-in URL : https://cloudbinary-dev.signin.aws.amazon.com/console
    User name : joel
    Console password : ***************

Permissions:
    - Admin (AdministratorAccess)
    - IAM

```

```
AWS CloudTrail
    Track user activity and API usage on AWS and in hybrid and multicloud environments


```

```
Sam:

Peter :


Tom: 

Console sign-in URL : https://cloudbinary-dev.signin.aws.amazon.com/console
User name : tom
Console password : NnMR*79!


Allow only Specific region based on company requirement and allow to only 1 service

{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "ec2:*",
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "aws:RequestedRegion": "us-east-1"
                }
            }
        }
    ]
}


```


#### IAM CLI 

```

AWS CLI Agent Download :

https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

AWS CLI Commands Help:

https://awscli.amazonaws.com/v2/documentation/api/latest/index.html


Login to AWS using :
    1. GUI(Mgmt Console)
    2. CLI (Programmatic)

Root user :

IAM User :

IAM Group :

IAM Policy :

IAM Permissions :

Enable MFA for IAM User

Task: Enable MFA for Root User 

```


#### Getting Started with Basics:

```
Cloud Computing Account Creation:
    1. AWS 

AWS Global Infrastructure:

Global Services vs Region:

Regions : 33 ; AZ's : 105 & CDN/PoPs/EdgeLocations : 600+ & 13 Regional edge caches

Global vs Region Services 

Analytics :
    1. Athena : Amazon Athena : Region Based Service
    2. AWS Clean Rooms

Application Integration
Blockchain
Business Applications
Cloud Financial Management
Compute
Containers
Customer Enablement
Database
Developer Tools
End User Computing
Front-end Web & Mobile
Game Development
Internet of Things
Machine Learning
Management & Governance
Media Services
Migration & Transfer
Networking & Content Delivery
Quantum Technologies
Robotics
Satellite
Security, Identity, & Compliance:
    1. IAM  : Global Service 
Storage

```
#### IAM 
```
IAM :
    - Groups
    - Users
    - Policy 

```

#### cloudbinary-only-iam-policy
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "iam:CreateUser",
                "iam:CreateLoginProfile",
                "iam:AddUserToGroup",
                "iam:AttachUserPolicy",
                "iam:PutUserPolicy",
                "iam:TagUser"
            ],
            "Resource": "*"
        }
    ]
}
```